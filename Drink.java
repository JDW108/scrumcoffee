package CoffeeShop;


import java.math.BigDecimal;

/**
 *
 * @author Robert W. Cline
 */

public class Drink {
    private String name;
    private BigDecimal price;
    private String size;
    private Toppings Toppings;
    private Toppings t;
    
    public Drink(boolean t){
        this.name = "";
        this.price = new BigDecimal(0);
        this.size = "";
        this.Toppings = new Toppings(true);
    }
            

    public Drink(String name, String size, String price, Toppings Toppings) {
        this.name = name;
        this.price = new BigDecimal(price);
        this.size = size;
        this.Toppings = Toppings;
    }
    
    public Drink(Drink c){
        this.name = c.name;
        this.price = c.price;
        this.size = c.size;
        this.Toppings = c.Toppings;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = new BigDecimal(price);
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Toppings getToppings() {
        return Toppings;
    }

    public void setToppings(Toppings Toppings) {
        t = new Toppings(Toppings);
        this.Toppings = t;
    }

    @Override
    public String toString(){
        return this.name + " " + this.price + "\n" + this.Toppings.toString();
    }
    
    public BigDecimal totalPrice(){
        return this.price.add(this.Toppings.totalPrice());
    }
    
}