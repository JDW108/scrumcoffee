package CoffeeShop;

//imports for the email part
import java.math.BigDecimal;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;


import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;

//@Author : Josh Wagner
//
public class CoffeeShopMenuController {

    //Initializing string arrays to be stored in comboboxes

    private final String[] coffeeTypes = {"HOT ESPRESSO", "ICED ESPRESSO", "ICED COFFEE/TEA", "COFFEE/TEA", "FRAPPUCCINO", "OTHER"};
    private final String[] hotEspressos = {"CAFFE LATTE", "CAPPUCCINO", "CAFFE MOCHA", "VANILLA LATTE", "CARAMEL MACCHIATO", "WHITE CHOCOLATE MOCHA", "CAFFE AMERICANO"};
    private final String[] icedEspressos = {"ICED CAFFE LATTE", "ICED CAFFE MOCHA", "ICED VANILLA LATTE", "ICED CARAMEL MACCHIATO", "ICED WHITE CHOCOLATE MOCHA", "ICED CAFFE AMERICANO"};
    private final String[] icedCoffeeTeas = {"ICED COFFEE", "COLD BREW COFFEE", "TEAVANA SHAKEN ICED TEA", "TEAVANA ICED CHAI LATTE"};
    private final String[] coffeeTeas = {"FRESH BREWED COFFEE", "CAFFE MISTO", "TEAVANA CHAI LATTE", "TEAVANA HOT BREWED TEA"};
    private final String[] frappucinos = {"COFFEE", "CARAMEL", "MOCHA", "JAVA CHIP", "WHITE CHOCOLATE MOCHA", "CAFFE VANILLA", "VANILLA BEAN CREME", "STRAWBERRIES & CREME", "DOUBLE CHOCOLATEY CHIP CREME", "WHITE CHOCOLATE CREME"};
    private final String[] others = {"HOT CHOCOLATE", "WHITE HOT CHOCOLATE", "STEAMER"};
    private final String[] sizes = {"GRANDE", "VENTI"};
    
    private String[] orderTimes;
    int orderCounter = 0;
    
    private String currentOrderTime;

    //Initializing Choice for each combobox to the first in each array
    private String sizeChoice = sizes[0];
    private String coffeeTypeChoice = coffeeTypes[0];
    private String coffeeNameChoice = hotEspressos[0];

    //Variable to hold the current selected string in the combo boxes
    private String currentSizeChoice;
    private String currentNameChoice;
    private String currentTypeChoice;
    private String currentSyrup;
    private String currentMilk;

    //Setting up the Obvservable list for each of the comboboxes options
    private ObservableList<String> sizeList = FXCollections.observableArrayList(sizes);
    private ObservableList<String> coffeeTypesList = FXCollections.observableArrayList(coffeeTypes);
    private ObservableList<String> hotEspressosList = FXCollections.observableArrayList(hotEspressos);
    private ObservableList<String> icedEspressosList = FXCollections.observableArrayList(icedEspressos);
    private ObservableList<String> icedCoffeeTeaList = FXCollections.observableArrayList(icedCoffeeTeas);
    private ObservableList<String> coffeeTeasList = FXCollections.observableArrayList(coffeeTeas);
    private ObservableList<String> frappucinosList = FXCollections.observableArrayList(frappucinos);
    private ObservableList<String> othersList = FXCollections.observableArrayList(others);

    //Change this from String to Drink or Toppings Later
    private ArrayList<String> alist = new ArrayList<>();
    private ArrayList<String> cart = new ArrayList<>();

    //Create Drink Var and Order Var and Topping
    private Order order = new Order(true);
    private Toppings toppings = new Toppings(false);
    private Drink drink = new Drink(true);
    private String price = "";
    private BigDecimal totalPrice = new BigDecimal(0);
    private int size = 0;
    
    @FXML
    private ResourceBundle resources;
    @FXML
    private URL location;
    @FXML
    private ComboBox<String> CoffeeTypeComboBox;
    @FXML
    private ComboBox<String> CoffeeNameComboBox;
    @FXML
    private Button SubmitOrderButton;
    @FXML
    private RadioButton VanillaSyrupRadioButton;
    @FXML    
    private RadioButton invisibleRadioButton1;
    @FXML
    private RadioButton invisibleRadioButton2;
    @FXML
    private ToggleGroup SyrupToggle;
    @FXML
    private RadioButton EspressoShotRadioButton;
    @FXML
    private RadioButton SoymilkRadioButton;
    @FXML
    private ToggleGroup milkToggle;
    @FXML
    private RadioButton AlmondmilkRadioButton;
    @FXML
    private RadioButton CoconutmilkRadioButton;
    @FXML
    private TextArea ReceiptTextField;
    @FXML
    private RadioButton ClassicSyrupRadioButton;
    @FXML
    private RadioButton CaramelSyrupRadioButton;
    @FXML
    private RadioButton CinnamonDolceSyrupRadioButton;
    @FXML
    private RadioButton HazelnutSyrupRadioButton;
    @FXML
    private RadioButton ToffeeNutSyrupRadioButton;
    @FXML
    private RadioButton PeppermintSyrupRadioButton;
    @FXML
    private RadioButton RaspberrySyrupRadioButton;
    @FXML
    private ComboBox<String> SizeComboBox;
    @FXML
    private RadioButton WhippedCreamRadioButton;
    @FXML
    private TextField NameTextField;
    @FXML
    private TextField PickupTimeTextBox;
    @FXML
    private TextField EmailTextField;
    @FXML
    private Button DeleteCartButton;
    @FXML
    private Button AddToCartButton;
    
    @FXML
    void AddToCartButtonPressed(ActionEvent event) {

        //Clear the reciept so that the header and footer only get printed once
        ReceiptTextField.clear();
        //Set total to 0 to start new
        totalPrice = new BigDecimal(0);
        
        

        //See if whipped and espr are set
        if (WhippedCreamRadioButton.isSelected()) {
            toppings.setWhippedCream(true);
        }
        if (EspressoShotRadioButton.isSelected()) {
            toppings.setEspressoShot(true);
        }

        //check which milk is selected
        if (AlmondmilkRadioButton.isSelected()) {
            toppings.setAlmondMilk(true);
        } else if (CoconutmilkRadioButton.isSelected()) {
            toppings.setCoconutMilk(true);
        } else if (SoymilkRadioButton.isSelected()) {
            toppings.setSoyMilk(true);
        }

        //check to see what syrup is selected
        if (CaramelSyrupRadioButton.isSelected()) {
            toppings.setCaramelSyrup();
        } else if (CinnamonDolceSyrupRadioButton.isSelected()) {
            toppings.setCinnamonDolceSyrup();
        } else if (ClassicSyrupRadioButton.isSelected()) {
            toppings.setClassicSyrup();
        } else if (HazelnutSyrupRadioButton.isSelected()) {
            toppings.setHazelnutSyrup();
        } else if (PeppermintSyrupRadioButton.isSelected()) {
            toppings.setPeppermintSyrup();
        } else if (ToffeeNutSyrupRadioButton.isSelected()) {
            toppings.setToffeNutSyrup();
        } else if (VanillaSyrupRadioButton.isSelected()) {
            toppings.setVanillaSyrup();
        }
        
        
        //Null Pointer can occur how ever so just default price if that happens
        try{
        //Set the price to what is selected
        if(currentTypeChoice.equals("FRAPPUCCINO") && currentSizeChoice.equals("GRANDE")){
            price = "4.99";
            if(currentNameChoice.equals("COFFEE")){
                price = "4.29";
            }
        } else if(currentTypeChoice.equals("FRAPPUCCINO") && currentSizeChoice.equals("VENTI")){
            price = "5.79";
            if(currentNameChoice.equals("COFFEE")){
                price = "5.09";
            }
        }
        
        //Set Others price
       if(currentTypeChoice.equals("OTHER") && currentSizeChoice.equals("GRANDE")){
            price = "3.29";
        } else if(currentTypeChoice.equals("OTHER") && currentSizeChoice.equals("VENTI")){
            price = "3.89";
        }
       
       //Set Coffee and Tea Price
       if((currentNameChoice.equals("FRESH BREWED COFFEE") || currentNameChoice.equals("TEAVANA HOT BREWED TEA")) && currentSizeChoice.equals("GRANDE")){
            price = "2.39";
        } else if((currentNameChoice.equals("FRESH BREWED COFFEE") || currentNameChoice.equals("TEAVANA HOT BREWED TEA")) && currentSizeChoice.equals("VENTI")){
            price = "2.69";
        }
       
       //Set Cafe Misto
       if((currentNameChoice.equals("CAFFE MISTO")) && currentSizeChoice.equals("GRANDE")){
            price = "2.79";
        } else if((currentNameChoice.equals("CAFFE MISTO")) && currentSizeChoice.equals("VENTI")){
            price = "3.19";
        }
       
       //Set Chai Latte and Chai latte
       if((currentNameChoice.equals("TEAVANA CHAI LATTE") || currentNameChoice.equals("TEAVANA ICED CHAI LATTE")) && currentSizeChoice.equals("GRANDE")){
            price = "4.39";
        } else if((currentNameChoice.equals("TEAVANA CHAI LATTE") || currentNameChoice.equals("TEAVANA ICED CHAI LATTE")) && currentSizeChoice.equals("VENTI")){
            price = "4.79";
        } 
       
       //Set Shanken Ice Tea
       if(currentNameChoice.equals("TEAVANA SHAKEN ICED TEA") && currentSizeChoice.equals("GRANDE")){
           price = "2.99";
       } else if(currentNameChoice.equals("TEAVANA SHAKEN ICED TEA") && currentSizeChoice.equals("VENTI")){
           price = "3.39";
       }
       
       //set cold brew coffee
       if(currentNameChoice.equals("COLD BREW COFFEE") && currentSizeChoice.equals("GRANDE")){
           price = "3.59";
       } else if(currentNameChoice.equals("COLD BREW COFFEE") && currentSizeChoice.equals("VENTI")){
           price = "4.09";
       }
       
       //set iced Coffee
       if(currentNameChoice.equals("ICED COFFEE") && currentSizeChoice.equals("GRANDE")){
           price = "2.59";
       } else if(currentNameChoice.equals("ICED COFFEE") && currentSizeChoice.equals("GRANDE")){
           price = "2.89";
       }
       
       //icedCaffe Americano
       if(currentNameChoice.equals("ICED CAFFE AMERICANO") && currentSizeChoice.equals("GRANDE")){
           price = "2.89";
       } else if(currentNameChoice.equals("ICED CAFFE AMERICANO") && currentSizeChoice.equals("VENTI")){
           price = "3.29";
       }
       
       //ice white moca
       if(currentNameChoice.equals("ICED WHITE CHOCOLATE MOCHA") && currentSizeChoice.equals("GRANDE")){
           price = "4.69";
       } else if(currentNameChoice.equals("ICED WHITE CHOCOLATE MOCHA") && currentSizeChoice.equals("VENTI")){
           price = "5.29";
       }
       
       //Iced Caramel Macc
       if(currentNameChoice.equals("ICED CARAMEL MACCHIATO") && currentSizeChoice.equals("GRANDE")){
           price = "5.39";
       } else if(currentNameChoice.equals("ICED CARAMEL MACCHIATO") && currentSizeChoice.equals("VENTI")){
           price = "5.79";
       }
       
       //iced vanilla latte
       if(currentNameChoice.equals("ICED VANILLA LATTE") && currentSizeChoice.equals("GRANDE")){
           price = "4.99";
       } else if(currentNameChoice.equals("ICED VANILLA LATTE") && currentSizeChoice.equals("VENTI")){
           price = "5.59";
       }
       //caffe mocha
       if(currentNameChoice.equals("ICED CAFFE MOCHA") && currentSizeChoice.equals("GRANDE")){
           price = "4.69";
       } else if(currentNameChoice.equals("ICED CAFFE MOCHA") && currentSizeChoice.equals("VENTI")){
           price = "5.29";
       }
       
       
       //Iced Caffe Latte
       if(currentNameChoice.equals("ICED CAFFE LATTE") && currentSizeChoice.equals("GRANDE")){
           price = "3.99";
       } else if(currentNameChoice.equals("ICED CAFFE LATTE") && currentSizeChoice.equals("VENTI")){
           price = "4.39";
       }
       
       //Cafe Americano
       if(currentNameChoice.equals("CAFFE AMERICANO") && currentSizeChoice.equals("GRANDE")){
           price = "2.79";
       } else if(currentNameChoice.equals("CAFFE AMERICANO") && currentSizeChoice.equals("VENTI")){
           price = "3.19";
       }
       
       //White Coco mocha
       if(currentNameChoice.equals("WHITE CHOCOLATE MOCHA") && currentSizeChoice.equals("GRANDE")){
           price = "4.69";
       } else if(currentNameChoice.equals("WHITE CHOCOLATE MOCHA") && currentSizeChoice.equals("VENTI")){
           price = "5.29";
       }
       
       //Caramel macha
       if(currentNameChoice.equals("CARAMEL MACCHIATO") && currentSizeChoice.equals("GRANDE")){
           price = "5.39";
       } else if(currentNameChoice.equals("CARAMEL MACCHIATO") && currentSizeChoice.equals("VENTI")){
           price = "5.79";
       }
       
       //Vanilla Latte
       if(currentNameChoice.equals("VANILLA LATTE") && currentSizeChoice.equals("GRANDE")){
           price = "4.99";
       } else if(currentNameChoice.equals("VANILLA LATTE") && currentSizeChoice.equals("VENTI")){
           price = "5.59";
       }
       
       //Caffe Mocha
       if(currentNameChoice.equals("CAFFE MOCHA") && currentSizeChoice.equals("GRANDE")){
           price = "4.69";
       } else if(currentNameChoice.equals("CAFFE MOCHA") && currentSizeChoice.equals("VENTI")){
           price = "5.29";
       }
       
       //Cappacino
       if((currentNameChoice.equals("CAPPUCCINO") || currentNameChoice.equals("CAFFE LATTE")) && currentSizeChoice.equals("GRANDE")){
           price = "3.89";
       } else if((currentNameChoice.equals("CAPPUCCINO") || currentNameChoice.equals("CAFFE LATTE")) && currentSizeChoice.equals("VENTI")){
           price = "4.39";
       }

        }catch(NullPointerException q){
            //If that error occurs just set the price to a default value
            
            price = "3.99";
        }
        /*
         else if(RaspberrySyrupRadioButton.isSelected()){
         toppings.setRaspberrySyrup(true);
         } for what ever reason this has a null pointer error
         */
        //This is an error case
        if (PickupTimeTextBox.getText().isEmpty()) {
            ReceiptTextField.setText("You MUST choose a Drink Type, Drink, Size, AND Pickup Time.");
            alist.clear();
            return;
        }

        //Getting the time from the user
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            String str = sdf.format(new Date());
            
            currentOrderTime = PickupTimeTextBox.getText() + "00";
            str = str.substring(0, 2) + str.substring(3, 5) + str.substring(6, 8);
            String formattedOrder1 = currentOrderTime.substring(0, 2) + ":" + currentOrderTime.substring(2, 4) + ":" + currentOrderTime.substring(4, 6);
            String formattedOrder2 = str.substring(0, 2) + ":" + str.substring(2, 4) + ":" + str.substring(4, 6);

            //Checking that the time entered by user is within business hours
            if (Double.valueOf(currentOrderTime) < 070000 || Double.valueOf(currentOrderTime) > 154500) {
                ReceiptTextField.setText("You MUST order at least 15 minutes in advance and you may not order for pickup outside"
                        + "of 7A.M - 3:45P.M.");
                return;
            } else if (currentOrderTime.equals(null)) {
                ReceiptTextField.setText("You have to enter a time");
            }
        //**********************THIS MIGHT NOT WORK KEEP AN EYE ON THIS ******************************************
            //*********CHECKS THAT WHILE WITHIN BUSINESS HOURS, ORDERS ARE ORDERED AT LEAST 15 MINUTES IN ADVANCE*****
        /*
             if(Double.valueOf(currentOrderTime) - Double.valueOf(str) < 150000 && Double.valueOf(str) < 1545 && Double.valueOf(str) > 070000){
             ReceiptTextField.setText("You MUST order at least 15 minutes in advance and you may not order for pickup outside"
             + "of 7A.M - 3:45P.M.");
             alist.clear();
             return;
             }
             */

        // print this once at the start
            //Printing Receipt
            ReceiptTextField.appendText("NAME: " + NameTextField.getText() + " , EMAIL: " + EmailTextField.getText() + "  ");
            ReceiptTextField.appendText(" ======================= ");

            //Setting the drinks name size and toppings and adding it to the order
            if (!order.isFull()) {
                drink.setName(currentNameChoice);
                drink.setSize(currentSizeChoice);
                drink.setPrice(price);
                drink.setToppings(toppings);
                order.add(drink);
                size = size + 1;
                
                if(order.isFull()){
                    //Disable the add button if the cart is full
                    AddToCartButton.setDisable(true);
                }
                
            } 
            
            
            //Try to append the order the receipt
            try {
                ReceiptTextField.appendText(order.toString());
                ReceiptTextField.appendText("  ");
            } catch (NullPointerException e) {
                ReceiptTextField.setText("You MUST choose a Drink Type, Drink, Size, AND Pickup Time.");
                alist.clear();
                return;
            }
            
            
                //add the price of the coffe then the toppings of that coffee
            for(int j = 0; j < order.order.size(); j++){
                totalPrice = totalPrice.add(order.order.get(j).totalPrice());
            }
            

            ReceiptTextField.appendText("Total: " + totalPrice + " ");
            
            
            //Print receipt. This looks ugly but prints ok.
            ReceiptTextField.appendText(" ======================= ");
            ReceiptTextField.appendText(" Order placed at : " + formattedOrder2 + "             ");
            ReceiptTextField.appendText(" Order will be picked up at : " + formattedOrder1 + " ");
            ReceiptTextField.appendText("  End of Order ");
            ReceiptTextField.appendText(" ======================= ");
            ReceiptTextField.appendText("                         ");

            
            //Set time text box to default after the user put it in once
            PickupTimeTextBox.setText(currentOrderTime.substring(0, 4));
            NameTextField.setText(NameTextField.getText());
            EmailTextField.setText(EmailTextField.getText());
            
            //Reset all the fields of the UI a user previously entered
            invisibleRadioButton1.setSelected(true);
            currentSyrup = "";
            invisibleRadioButton2.setSelected(true);
            currentMilk = "";
            EspressoShotRadioButton.setSelected(false);
            WhippedCreamRadioButton.setSelected(false);
            CoffeeTypeComboBox.setItems(coffeeTypesList);
            SizeComboBox.setItems(sizeList);
        } catch (NullPointerException e) {
            
        } catch (StringIndexOutOfBoundsException j) {
            System.out.println("The number you gave was out of bounds");
        }
        
        //Set the delete and submit buttons to enables because stuff exists within the cart
        DeleteCartButton.setDisable(false);
        SubmitOrderButton.setDisable(false);
        
        //Clear the toppings so that repeats dont get though
        toppings.clear();
        
    }
    
    @FXML
    void CoffeeNameComboBoxPressed(ActionEvent event) {
        coffeeNameChoice = currentNameChoice;
    }
    
    @FXML
    void CoffeeTypeComboBoxPressed(ActionEvent event) {
        try {
            if (currentTypeChoice.equals(coffeeTypes[0])) {
                CoffeeNameComboBox.setItems(hotEspressosList);
            } else if (currentTypeChoice.equals(coffeeTypes[1])) {
                CoffeeNameComboBox.setItems(icedEspressosList);
            } else if (currentTypeChoice.equals(coffeeTypes[2])) {
                CoffeeNameComboBox.setItems(icedCoffeeTeaList);
            } else if (currentTypeChoice.equals(coffeeTypes[3])) {
                CoffeeNameComboBox.setItems(coffeeTeasList);
            } else if (currentTypeChoice.equals(coffeeTypes[4])) {
                CoffeeNameComboBox.setItems(frappucinosList);
            } else if (currentTypeChoice.equals(coffeeTypes[5])) {
                CoffeeNameComboBox.setItems(othersList);
            } else {
                //Something bad happened.
            }
        } catch (NullPointerException e) {
            
        }
    }
    
    @FXML
    void DeleteCartButtonPressed(ActionEvent event) {
        //Set add button to enabled because the cart is nolonger full
        AddToCartButton.setDisable(false);
        
        //Clear the order and the text inthe receipt
        order.order.clear();
        ReceiptTextField.clear();
        
        //Set remove and confirm to disabled because nothing exist within the order
        DeleteCartButton.setDisable(true);
        SubmitOrderButton.setDisable(true);
        
        size = 0;
    }
    
    @FXML
    void onAlmondmilkRadioButtonSelected(ActionEvent event) {
        currentMilk = "Almond Milk";
    }
    
    @FXML
    void onCaramelSyrupRadioButtonSelected(ActionEvent event) {
        currentSyrup = "Caramel Syrup";
    }
    
    @FXML
    void onCinnamonDolceSyrupRadioButtonSelected(ActionEvent event) {
        currentSyrup = "CinnamonDolce";
    }
    
    @FXML
    void onClassicSyrupRadioButtonSelected(ActionEvent event) {
        currentSyrup = "Classic Syrup";
    }
    
    @FXML
    void onCoconutmilkRadioButtonSelected(ActionEvent event) {
        currentMilk = "Coconut Milk";
    }
    
    @FXML
    void onEspressoShotRadioButtonSelected(ActionEvent event) {
        
    }
    
    @FXML
    void onVanillaSyrupRadioButtonSelected(ActionEvent event) {
        currentSyrup = "Vanilla Syrup";
    }
    
    @FXML
    void onHazelnutSyrupRadioButtonSelected(ActionEvent event) {
        currentSyrup = "Hazelnut Syrup";
    }
    
    @FXML
    void onPepperminSyrupRadioButtonSelected(ActionEvent event) {
        currentSyrup = "Peppermint Syrup";
    }
    
    @FXML
    void onRasperrySyrupRadioButtonSelected(ActionEvent event) {
        currentSyrup = "Raspberry Syrup";
    }

    @FXML
    void onInvisibleRadioButton1Selected(ActionEvent event) {
        currentSyrup = "No Syrup";
    }

    @FXML
    void onInvisibleRadioButton2Selected(ActionEvent event) {
        currentSyrup = "No Syrup";
    }
    
    @FXML
    void onSizeComboBoxPressed(ActionEvent event) {
        
    }
    
    @FXML
    void onSoymilkRadioButtonSelected(ActionEvent event) {
        currentMilk = "Soymilk";
    }
    
    @FXML
    void onSubmitOrderButtonPressed(ActionEvent event) {

        //Trash Gmail account
	//Login: swengfortnite1@gmail.com
	//Password: fortnite1
            
        //Other trash account to send
        //Login: ProgramSendersweng@gmail.com
        //Password: fortnite1
            
        /**
         * Okay so here is the thing there is alot you need to do for this program to
         * work on your computer first things first download  JavaMail API 1.4.7 and unzip it
         * download Java Activation Framework (JAF (Version 1.1.1)) unzip it too then follow this links
         * instruction to add the jars to your classpath
         * https://javaee.github.io/javamail/FAQ#classpathwin
         */
	
	//Set up the recepient email
	String recepient = "swengfortnite1@gmail.com";
	
	//Set up the sender, we will have to change this to what the user inputs
	String sender = "ProgramSendersweng";
        
        String password = "fortnite1";
	
	//Assume local host
	String host = "smtp.gmail.com";
	
	//Get system properties
	Properties properties = System.getProperties();
	
	//Set up mail server
	properties.setProperty("smtp.gmail.com", host);
        properties.put("mail.smtp.auth","true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");
	
	// Get the default Session object.
    Session session = Session.getDefaultInstance(properties, new Authenticator(){
        @Override
        protected PasswordAuthentication getPasswordAuthentication(){
            return new PasswordAuthentication(sender, password);
        }
    });
	
	
	//Set up try catch block to catch to catch if the message doesnt send
	try {
         // Create a default MimeMessage object.
         MimeMessage message = new MimeMessage(session);

         // Set From: header field of the header.
         message.setFrom(new InternetAddress(sender));

         // Set To: header field of the header.
         message.addRecipient(Message.RecipientType.TO, new InternetAddress(recepient));

         // Set Subject: header field this will just say something about coffee order
         message.setSubject(NameTextField.getText() + "'s Coffee Order");

         // Now set the actual message this will contain the order itself
         message.setText(order.toString());

         // Send message
         Transport.send(message);
		 
		 
      } catch (MessagingException mex) {
         mex.printStackTrace();
      }
        
        System.exit(1);
        
    }
    
    @FXML
    void onToffeeNutSyrupRadioButtonSelected(ActionEvent event) {
        currentSyrup = "ToffeeNut Syrup";
    }
    
    @FXML
    void onWhippedCreamRadioButtonSelected(ActionEvent event) {
        
    }
    
    @FXML
    void initialize() {
        assert CoffeeTypeComboBox != null : "fx:id=\"CoffeeTypeComboBox\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert CoffeeNameComboBox != null : "fx:id=\"CoffeeNameComboBox\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert SubmitOrderButton != null : "fx:id=\"SubmitOrderButton\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert VanillaSyrupRadioButton != null : "fx:id=\"FlavoredSyrupRadioButton\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert SyrupToggle != null : "fx:id=\"SyrupToggle\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert EspressoShotRadioButton != null : "fx:id=\"EspressoShotRadioButton\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert SoymilkRadioButton != null : "fx:id=\"SoymilkRadioButton\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert milkToggle != null : "fx:id=\"milkToggle\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert AlmondmilkRadioButton != null : "fx:id=\"AlmondmilkRadioButton\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert CoconutmilkRadioButton != null : "fx:id=\"CoconutmilkRadioButton\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert ReceiptTextField != null : "fx:id=\"ReceiptTextField\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert ClassicSyrupRadioButton != null : "fx:id=\"ClassicSyrupRadioButton\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert CaramelSyrupRadioButton != null : "fx:id=\"CaramelSyrupRadioButton\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert CinnamonDolceSyrupRadioButton != null : "fx:id=\"CinnamonDolceSyrupRadioButton\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert HazelnutSyrupRadioButton != null : "fx:id=\"HazelnutSyrupRadioButton\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert ToffeeNutSyrupRadioButton != null : "fx:id=\"ToffeeNutSyrupRadioButton\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert PeppermintSyrupRadioButton != null : "fx:id=\"PeppermintSyrupRadioButton\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert RaspberrySyrupRadioButton != null : "fx:id=\"RasperrySyrupRadioButton\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert SizeComboBox != null : "fx:id=\"SizeComboBox\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert WhippedCreamRadioButton != null : "fx:id=\"WhippedCreatRadioButton\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert NameTextField != null : "fx:id=\"NameTextField\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert EmailTextField != null : "fx:id=\"EmailTextField\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert DeleteCartButton != null : "fx:id=\"DeleteCartButton\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        assert AddToCartButton != null : "fx:id=\"AddToCartButton\" was not injected: check your FXML file 'CoffeeShopMenu.fxml'.";
        
        
        //Set remove and confirm to disabled at the start
        DeleteCartButton.setDisable(true);
        SubmitOrderButton.setDisable(true);
        
        
        
        CoffeeTypeComboBox.setItems(coffeeTypesList);
        
        CoffeeTypeComboBox.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String oldValue, String newValue) {
                currentTypeChoice = newValue;
            }
        }
        );
        
        CoffeeNameComboBox.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String oldValue, String newValue) {
                currentNameChoice = newValue;
            }
        }
        );
        
        SizeComboBox.setItems(sizeList);
        
        SizeComboBox.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String oldValue, String newValue) {
                currentSizeChoice = newValue;
            }
        }
        );
        
    }
}
