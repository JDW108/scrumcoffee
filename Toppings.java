package CoffeeShop;
import java.math.BigDecimal;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rachel
 */
public class Toppings {
    
    private boolean vanillaSyrup=false;
    private boolean classicSyrup=false;
    private boolean caramelSyrup=false;
    private boolean hazelnutSyrup=false;
    private boolean toffeeNutSyrup=false;
    private boolean raspberrySyrup=false;
    private boolean peppermintSyrup=false;
    private boolean cinnamonDolceSyrup=false;
    private boolean syrup=false;
    private BigDecimal syrupPrice = new BigDecimal(".99");
    
    private boolean espressoShot=false;
    private BigDecimal espressoPrice = new BigDecimal("1.79");
    
    private boolean whippedCream=false;
    private BigDecimal whippedCreamPrice = new BigDecimal("0.00");
    
    private boolean soyMilk=false;
    private boolean almondMilk=false;
    private boolean coconutMilk=false;
    private BigDecimal milkPrice = new BigDecimal(".99");
    
    public Toppings(boolean t){
        soyMilk=false;
        almondMilk=false;
        coconutMilk=false;
        whippedCream=false;
        espressoShot=false;
        cinnamonDolceSyrup=false;
        vanillaSyrup=false;
        classicSyrup=false;
        caramelSyrup=false;
        hazelnutSyrup=false;
        toffeeNutSyrup=false;
        raspberrySyrup=false;
        peppermintSyrup=false;
    }
    public Toppings(Toppings t){
        soyMilk=t.soyMilk;
        almondMilk=t.almondMilk;
        coconutMilk=t.coconutMilk;
        whippedCream=t.whippedCream;
        espressoShot=t.espressoShot;
        cinnamonDolceSyrup=t.cinnamonDolceSyrup;
        vanillaSyrup=t.vanillaSyrup;
        classicSyrup=t.classicSyrup;
        caramelSyrup=t.caramelSyrup;
        hazelnutSyrup=t.hazelnutSyrup;
        toffeeNutSyrup=t.toffeeNutSyrup;
        raspberrySyrup=t.raspberrySyrup;
        peppermintSyrup=t.peppermintSyrup;
    }
    
    @Override
    public String toString(){
        String toppingsList="";
        
            if (vanillaSyrup)
                toppingsList+="Vanilla Syrup, " + syrupPrice + "\n";
            else if (classicSyrup)
                toppingsList+="Classic Syrup, "+ syrupPrice + "\n";
            else if (caramelSyrup)
                toppingsList+="Caramel Syrup, "+ syrupPrice + "\n";
            else if (hazelnutSyrup)
                toppingsList+="Hazelnut Syrup, "+ syrupPrice + "\n";
            else if (toffeeNutSyrup)
                toppingsList+="Toffe Nut Syrup, "+ syrupPrice + "\n";
            else if (raspberrySyrup)
                toppingsList+="Raspberry Syrup, "+ syrupPrice + "\n";
            else if (peppermintSyrup)
                toppingsList+="Peppermint Syrup, "+ syrupPrice + "\n";
            else if(cinnamonDolceSyrup)
                toppingsList+="Cinnamon Dolce Syrup, "+ syrupPrice + "\n";
        
        if (whippedCream)
            toppingsList+="Whipped cream, "+ whippedCreamPrice + "\n";
        if (espressoShot)
            toppingsList+="Espresso shot, "+ espressoPrice + "\n";
        if (soyMilk||almondMilk||coconutMilk){
            if (soyMilk)
                toppingsList+="Soy milk "+ milkPrice + "\n";
            else if (almondMilk)
                toppingsList+="Almond milk " + milkPrice + "\n";
            else// (coconutMilk)
                toppingsList+="Coconut milk "+ milkPrice + "\n";
        }
        return toppingsList;
    }
    
    public BigDecimal totalPrice(){
        BigDecimal total = new BigDecimal("0");
        if (vanillaSyrup||classicSyrup||caramelSyrup||hazelnutSyrup||toffeeNutSyrup||raspberrySyrup||peppermintSyrup||cinnamonDolceSyrup)
            total = total.add(syrupPrice);
        if (espressoShot)
            total = total.add(espressoPrice);
        if (soyMilk||almondMilk||coconutMilk)
            total = total.add(milkPrice);
        return total;
    }

    //syrup setters
    public void setVanillaSyrup( ) {
        vanillaSyrup = true;
        syrup=true;
        classicSyrup=caramelSyrup=hazelnutSyrup=toffeeNutSyrup=raspberrySyrup=peppermintSyrup=cinnamonDolceSyrup=false;
    }

    public void setClassicSyrup( ) {
        classicSyrup = true;
        syrup=true;
        vanillaSyrup=caramelSyrup=hazelnutSyrup=toffeeNutSyrup=raspberrySyrup=peppermintSyrup=cinnamonDolceSyrup=false;
    }

    public void setCaramelSyrup( ) {
        caramelSyrup = true;
        syrup=true;
        vanillaSyrup=classicSyrup=hazelnutSyrup=toffeeNutSyrup=raspberrySyrup=peppermintSyrup=cinnamonDolceSyrup=false;
    }

    public void setHazelnutSyrup( ) {
        hazelnutSyrup = true;
        syrup=true;
        vanillaSyrup=classicSyrup=caramelSyrup=toffeeNutSyrup=raspberrySyrup=peppermintSyrup=cinnamonDolceSyrup=false;
    }

    public void setToffeNutSyrup( ) {
        toffeeNutSyrup = true;
        syrup=true;
        vanillaSyrup=classicSyrup=caramelSyrup=hazelnutSyrup=raspberrySyrup=peppermintSyrup=cinnamonDolceSyrup=false;
    }

    public void setRaspberrySyrup( ) {
        raspberrySyrup = true;
        syrup=true;
        vanillaSyrup=classicSyrup=caramelSyrup=hazelnutSyrup=toffeeNutSyrup=peppermintSyrup=cinnamonDolceSyrup=false;
    }

    public void setPeppermintSyrup( ) {
        peppermintSyrup = true;
        syrup=true;
        vanillaSyrup=classicSyrup=caramelSyrup=hazelnutSyrup=toffeeNutSyrup=raspberrySyrup=cinnamonDolceSyrup=false;
    }

    public void setCinnamonDolceSyrup( ) {
        cinnamonDolceSyrup = true;
        syrup=true;
        vanillaSyrup=classicSyrup=caramelSyrup=hazelnutSyrup=toffeeNutSyrup=raspberrySyrup=peppermintSyrup=false;
    }

    //espresso setter
    public void setEspressoShot(boolean espressoShot) {
        this.espressoShot = espressoShot;
    }

    //whip setter
    public void setWhippedCream(boolean whippedCream) {
        this.whippedCream = whippedCream;
    }

    //milk setters
    public void setSoyMilk(boolean soyMilk) {
        this.soyMilk = soyMilk;
        almondMilk=coconutMilk=false;
    }

    public void setAlmondMilk(boolean almondMilk) {
        this.almondMilk = almondMilk;
        coconutMilk=soyMilk=false;
    }

    public void setCoconutMilk(boolean coconutMilk) {
        this.coconutMilk = coconutMilk;
        soyMilk=almondMilk=false;
    }
    
    public void clear(){
        soyMilk=false;
        almondMilk=false;
        coconutMilk=false;
        whippedCream=false;
        espressoShot=false;
        cinnamonDolceSyrup=false;
        vanillaSyrup=false;
        classicSyrup=false;
        caramelSyrup=false;
        hazelnutSyrup=false;
        toffeeNutSyrup=false;
        raspberrySyrup=false;
        peppermintSyrup=false;
    }
}
