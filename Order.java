/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoffeeShop;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

/**
 *
 * @author logan
 */
public class Order 
{
    String name;
    Date pickUp;
    ArrayList<Drink> order = new ArrayList<>();
    
    private Drink d;
    
    public Order()
    {
       this.name = ""; 
       this.pickUp = new Date();
       this.order = new ArrayList<>(5);
    }
    
    public Order(boolean r){
        this.name = ""; 
       this.pickUp = new Date();
       this.order = new ArrayList<>(5);
    }
    
    public Order(String s, Date p, ArrayList<Drink> o)
    {
        this.name = s;
        this.pickUp = p;
        this.order = o;
    }
    
    
    //getters setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getPickUp() {
        return pickUp;
    }

    public void setPickUp(Date pickUp) {
        this.pickUp = pickUp;
    }

    public List<Drink> getOrder() {
        return order;
    }

    public void setOrder(ArrayList<Drink> order) {
        this.order = order;
    }
    
    // call by Order.add(coffee)
    public void add(Drink c)
    {
        d = new Drink(c);
        if(this.isFull())
        {
            // do nothing therere cannot be more than 5 coffees in a list
        }
        else 
        {
            this.order.add(d);
        }
       
    }
    
    // for later
    //public void edit(Coffee c)
    
    
    // call by Order.delete(Coffee)
    public void delete(Drink c)
    {
        if(this.getOrder().contains(c))
        {
            this.getOrder().remove(c);
        }
    }
    
   
    
    // call by order.clear()
    public  void clear()
    {
        this.getOrder().clear();
        
    }
    
    // call by order.isFull()
    // returns true if list is 5 or grater
    public boolean isFull()
    {
          if(this.order.size() >= 5)
        {
            return true;
        }
        else 
        {
            return false;
        }
    }
    
    @Override
    public String toString(){
        String o = "";
        for(int i = 0; i < order.size(); i++){
            o = o + order.get(i).toString() + "\n" + " ======================= " + "\n";
            
        }
        return o;
    }
    
    
}
